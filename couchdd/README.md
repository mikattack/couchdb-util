# couchdd

Builds and updates CouchDB design documents from a manifest file.

Writing design documents is tedious and error prone.  Furthermore, it's ripe
for automation via a CLI tool.  Though there are many such tools out there,
most manage design documnets as part of a project, organize based on a
directory structure convention, or are written in NodeJS.

This tool fits a different niche.  It builds only a single design document
that is defined by a TOML-formated manifest file.

## Usage

First, write a manifest file that describes your design document, where it's stored, and
points to code which implements it.  Here's an example with code files all over the place.

`./manifest.toml`:

    # Document identifier
    id = "my-design-doc"
    
    # Storage details
    db = "my-db"
    host = "http://localhost:5984"
    
    # Path to code implementing design document features
    [lists]
    as-json = "formatted/as-json.js"
    as-xml  = "formatted/as-xml.js"
    
    # Path(s) to named update functions, one per file
    [updates]
    filtered-write = "src/writers/filtered/couchdb.js"
    
    [views.customers]
    map    = "src/common/customers/map.js"
    reduce = "src/common/customers/reduce.js"
    
    [views.organizations]
    map    = "src/common/organization/map"
    reduce = "src/common/organization/reduce"


Next, run the tool.  It will read the manifest file, figure out where things are stored, build
the design document, and update it.

    couchdd

The tool is simplistic and doesn't have many features (by intent).  Still, check the help
output to see the few options available.
